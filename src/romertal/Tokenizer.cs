﻿using System;

namespace Romertal
{
    public interface ITokenizer<out TToken> where TToken : class 
    {
        TToken GetNext();
        TToken PeekAt(int steps);
        TToken PeekCurr();
        TToken PeekPrev();
        TToken PeekNext();
    }

    public class Tokenizer<TToken> : ITokenizer<TToken> where TToken : class
    {
        private readonly string _s;
        private int _index = -1;
        private readonly Func<char, TToken> _tokenizer;

        public Tokenizer(string s, Func<char, TToken> tokenizer)
        {
            _s = s;
            _tokenizer = tokenizer;
        }

        public TToken GetNext()
        {
            if (_index + 1 < _s.Length)
            {
                _index += 1;
                return _tokenizer(_s[_index]);
            }
            return null;
        }

        public TToken PeekAt(int steps)
        {
            int i = _index + steps;
            if (0 <= i && i < _s.Length)
            {
                return _tokenizer(_s[i]);
            }
            return null;
        }

        public TToken PeekCurr()
        {
            return PeekAt(0);
        }

        public TToken PeekPrev()
        {
            return PeekAt(-1);
        }

        public TToken PeekNext()
        {
            return PeekAt(1);
        }
    }
}
