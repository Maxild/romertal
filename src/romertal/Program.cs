﻿using System;
using System.Linq;
using System.Reflection;

namespace Romertal
{
    static class Program
    {
        /// <summary>
        /// Entry point (CLR on windows will call you here)
        /// </summary>
        static int Main(string[] args)
        {
            try
            {
                string s = args.FirstOrDefault();

                if (Converter.ContainsDecTokens(s))
                {
                    Console.WriteLine(Converter.DecToRoman(s));
                }
                else if (Converter.ContainsRomanTokens(s))
                {
                    Console.WriteLine(Converter.RomanToDec(s));
                }
                else
                {
                    Console.WriteLine("Input is neither decimal number or roman number -- try again:-)");
                }
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine($"Invalid input: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine("UNEXPECTED ERROR");
                Console.WriteLine($"{ex.GetType().GetTypeInfo().Name}: {ex.Message}");
            }

            return 0;
        }
    }
}
