﻿using System;
using System.Linq;
using System.Text;

namespace Romertal
{
    public static class Converter
    {
        public static bool ContainsDecTokens(string s)
        {
            if (string.IsNullOrEmpty(s)) return false;
            return s.All(char.IsDigit);
        }

        // Det stoerste tal vi kan skrive MMMDDDCCCLLLXXXVVVIII = 3000 + 1500 +

        public static string DecToRoman(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            if (false == ContainsDecTokens(s))
            {
                throw new FormatException(
                    $"'{s}' does not contain valid decimal tokens -- ({string.Join(", ", Enumerable.Range(0,9))} are the only valid characters.");
            }

            StringBuilder sb = new StringBuilder();

            int value = int.Parse(s);

            //
            // 1000
            //

            int thousands = RomanToken.M.Factor(value);
            if (thousands > 3)
            {
                throw new InvalidOperationException("Values above 3999 is not supported.");
            }
            // insert 'MMM', 'MM', 'M' or nothing
            sb.Append(RomanToken.M.Repeat(thousands));
            value -= RomanToken.M.Multiplied(thousands);

            // value is now below 1000

            //
            // 500
            //

            int fivehundreds = RomanToken.D.Factor(value);
            int hundreds = RomanToken.C.Factor(value);
            if (hundreds < 9)
            {
                // insert 'D' or nothing
                sb.Append(RomanToken.D.Repeat(fivehundreds));
                value -= RomanToken.D.Multiplied(fivehundreds);
            }
            else
            {
                // insert CM = 900
                sb.Append(RomanToken.C).Append(RomanToken.M);
                value -= RomanToken.C.Multiplied(hundreds); // 900
            }

            // value is now below 500

            //
            // 100
            //

            hundreds = RomanToken.C.Factor(value);
            if (hundreds < 4)
            {
                // insert 'CCC', 'CC', 'C' or nothing
                sb.Append(RomanToken.C.Repeat(hundreds));
            }
            else
            {
                // insert XC = 400
                sb.Append(RomanToken.X).Append(RomanToken.C);
            }
            value -= RomanToken.C.Multiplied(hundreds);

            // value is now below 100

            //
            // 50
            //

            int fiftys = RomanToken.L.Factor(value);
            int tens = RomanToken.X.Factor(value);
            if (tens < 9)
            {
                // insert 'L' or nothing
                sb.Append(RomanToken.L.Repeat(fiftys));
                value -= RomanToken.L.Multiplied(fiftys);
            }
            else
            {
                // insert XC = 90
                sb.Append(RomanToken.X).Append(RomanToken.C);
                value -= RomanToken.C.Multiplied(tens); // 90
            }

            // value is now below 50

            //
            // 10
            //

            tens = RomanToken.X.Factor(value);
            if (tens < 4)
            {
                // insert XXX, XX, X or nothing
                sb.Append(RomanToken.X.Repeat(tens));
            }
            else
            {
                // insert XL = 40
                sb.Append(RomanToken.X).Append(RomanToken.L);
            }
            value -= RomanToken.X.Multiplied(tens); // 40

            // value is below 10

            //
            // 5
            //

            int fives = RomanToken.V.Factor(value);
            int ones = value;
            if (ones < 9)
            {
                // insert V or nothing
                sb.Append(RomanToken.V.Repeat(fives));
                value -= RomanToken.V.Multiplied(fives);
            }
            else
            {
                // insert IX = 9
                sb.Append(RomanToken.I).Append(RomanToken.X);
                value -= RomanToken.I.Multiplied(ones); // 9
            }

            // value is below 5

            ones = value;
            if (ones < 4)
            {
                // insert III, II, I or nothing
                sb.Append(RomanToken.I.Repeat(ones));
            }
            else
            {
                // insert IV = 4
                sb.Append(RomanToken.I).Append(RomanToken.V);
            }
            value -= RomanToken.I.Multiplied(ones); // 4

            // Contracts better here
            if (value != 0)
            {
                throw new InvalidOperationException("Unexpected calculation error");
            }

            return sb.ToString();
        }

        public static bool ContainsRomanTokens(string s)
        {
            if (string.IsNullOrEmpty(s)) return false;
            return s.All(c => RomanToken.FromCharOrDefault(c) != null);
        }

        enum RomanStates
        {
            /// <summary>
            /// Only one digit have been read
            /// </summary>
            Unknown = 0,
            LessThanLastDigit,
            EqualToLastDigit,
            GreaterThanLastDigit
        }

        public static string RomanToDec(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            if (false == ContainsRomanTokens(s))
            {
                throw new FormatException(
                    $"'{s}' does not contain valid roman tokens -- ({string.Join(", ", RomanToken.GetAll())}) are the only valid characters.");
            }

            int value = 0;
            var tokenizer = RomanToken.GetTokenizerFor(s);
            int index = 0;
            int countIdenticalPairs = 0;

            RomanToken currToken;
            while (null != (currToken = tokenizer.GetNext()))
            {
                var nextToken = tokenizer.PeekNext();

                if (nextToken != null && currToken.CompareTo(nextToken) < 0)
                {
                    if (false == currToken.CanComeBeforeNextGreater(nextToken))
                    {
                        throw new InvalidOperationException($"The two ascending digits '{currToken}{nextToken}' at index {index} are not a valid pair.");
                    }

                    var prevToken = tokenizer.PeekPrev();

                    if (prevToken != null && prevToken.Equals(currToken))
                    {
                        throw new InvalidOperationException($"Only one identical smaller digit must be found in front of a larger roman digit. The problem is at index {index - 1}.");
                    }

                    // Hvis et mindre romertal skrives før et større tal, trækkes tallet fra det store
                    value += (tokenizer.GetNext().Value - currToken.Value); // subtraction

                    index += 1;
                    prevToken = tokenizer.PeekPrev();
                    currToken = tokenizer.PeekCurr();
                    nextToken = tokenizer.PeekNext();

                    if (nextToken != null && currToken.CompareTo(nextToken) < 0)
                    {
                        throw new InvalidOperationException($"An ascending sequence of roman digits '{prevToken}{currToken}{nextToken}' of length greater than 2 have been found at index {index - 1}. Only (some) ascending pairs are allowed.");
                    }

                    countIdenticalPairs = 0;
                }
                else
                {
                    if (currToken.Equals(tokenizer.PeekPrev()))
                    {
                        if (false == currToken.CanBeRepeatedUptil3Times)
                        {
                            throw new InvalidOperationException($"The roman digit '{currToken}' cannot be repeated at index {index - 1}.");
                        }
                        countIdenticalPairs += 1;
                    }
                    else
                    {
                        countIdenticalPairs = 0;
                    }

                    if (countIdenticalPairs > 2)
                    {
                        throw new InvalidOperationException($"The roman digit '{currToken}' cannot be repeated more then 3 times at index {index - 3}.");
                    }

                    // Hvis et mindre romertal skrives efter et stort romertal, lægges tallet til det store
                    // Hvis det samme romertal skrives efter hinanden, lægges tallet til..
                    value += currToken.Value; // addition
                }

                index += 1;
            }

            return value.ToString();
        }
    }
}
