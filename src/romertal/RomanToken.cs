﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Romertal
{
    public class RomanToken : IEquatable<RomanToken>, IComparable<RomanToken>
    {
        /// <summary>
        /// Roman 1
        /// </summary>
        public static readonly RomanToken I = new RomanToken('I', 1, true);
        /// <summary>
        /// Roman 5
        /// </summary>
        public static readonly RomanToken V = new RomanToken('V', 5, false);
        /// <summary>
        /// Roman 10
        /// </summary>
        public static readonly RomanToken X = new RomanToken('X', 10, true);
        /// <summary>
        /// Roman 50
        /// </summary>
        public static readonly RomanToken L = new RomanToken('L', 50, false);
        /// <summary>
        /// Roman 100
        /// </summary>
        public static readonly RomanToken C = new RomanToken('C', 100, true);
        /// <summary>
        /// Roman 500
        /// </summary>
        public static readonly RomanToken D = new RomanToken('D', 500, false);
        /// <summary>
        /// Roman 1000
        /// </summary>
        public static readonly RomanToken M = new RomanToken('M', 1000, true);

        public static ITokenizer<RomanToken> GetTokenizerFor(string s)
        {
            return new Tokenizer<RomanToken>(s, FromChar);
        }

        public static IEnumerable<RomanToken> GetAll()
        {
            yield return I;
            yield return V;
            yield return X;
            yield return L;
            yield return C;
            yield return D;
            yield return M;
        }

        public static RomanToken FromChar(char c)
        {
            var token = FromCharOrDefault(c);
            if (token == null)
            {
                throw new InvalidOperationException($"The character '{c}' is not a valid decimal number.");
            }
            return token;
        }

        public static RomanToken FromCharOrDefault(char c)
        {
            return GetAll().FirstOrDefault(t => t.Name.Equals(c));
        }



        private RomanToken(char name, int value, bool canBeRepeated)
        {
            Name = name;
            Value = value;
            CanBeRepeatedUptil3Times = canBeRepeated;
        }

        public char Name { get; }

        public int Value { get; }

        public bool CanBeRepeatedUptil3Times { get; } // I, X, C and M

        public bool CanBeLessThanPrevToken => CanBeRepeatedUptil3Times && Equals(M) == false; // I, X and C

        /// <summary>
        /// True if this RomanTokens is allowed to come before the other greater RomanTokens
        /// </summary>
        public bool CanComeBeforeNextGreater(RomanToken nextGreaterToken)
        {
            // IV, IX, XL, XC, CD, CM
            if (CompareTo(nextGreaterToken) < 0)
            {
                var list = GetAll().ToList();
                int index = list.IndexOf(this);
                return CanBeLessThanPrevToken &&
                    (nextGreaterToken.Equals(list[index + 1]) || nextGreaterToken.Equals(list[index + 2]));
            }
            return true;

        }

        /// <summary>
        /// The largest number of times the value of the RomanTokens can be multiplied to a number below the given value.
        /// </summary>
        public int Factor(int value)
        {
            return value / Value;
        }

        public string Repeat(int count)
        {
            return count <= 0 ? string.Empty : new string(Name, count);
        }

        public int Multiplied(int times)
        {
            return times * Value;
        }

        public int CompareTo(RomanToken other)
        {
            if (other == null) return -1;
            return Value.CompareTo(other.Value);
        }

        public override string ToString()
        {
            return Name.ToString();
        }

        public bool Equals(RomanToken other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Value == other.Value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RomanToken)obj);
        }

        public override int GetHashCode()
        {
            return Value;
        }
    }
}
