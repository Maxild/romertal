﻿using System;
using Romertal;
using Xunit;

namespace UnitTests
{
    public class TokenizerTests
    {
        [Fact]
        public void GetNext()
        {
            var sut = new Tokenizer<RomanToken>("MCII", RomanToken.FromChar);

            var token = sut.GetNext();
            Assert.Null(sut.PeekPrev());
            Assert.Null(sut.PeekAt(-2));
            Assert.Equal(RomanToken.C, sut.PeekNext());
            Assert.Equal(RomanToken.M, token);

            token = sut.GetNext();
            Assert.Equal(RomanToken.M, sut.PeekPrev());
            Assert.Null(sut.PeekAt(-2));
            Assert.Equal(RomanToken.I, sut.PeekNext());
            Assert.Equal(RomanToken.C, token);

            token = sut.GetNext();
            Assert.Equal(RomanToken.C, sut.PeekPrev());
            Assert.Equal(RomanToken.M, sut.PeekAt(-2));
            Assert.Equal(RomanToken.I, sut.PeekNext());
            Assert.Equal(RomanToken.I, token);

            token = sut.GetNext();
            Assert.Equal(RomanToken.I, sut.PeekPrev());
            Assert.Equal(RomanToken.C, sut.PeekAt(-2));
            Assert.Null(sut.PeekNext());
            Assert.Equal(RomanToken.I, token);

            Assert.Null(sut.GetNext());
        }

        [Fact]
        public void GetNext_InvalidNumber_ThrowsInvalidOperation()
        {
            var sut = new Tokenizer<RomanToken>("MCaII", RomanToken.FromChar);
            Assert.Throws<InvalidOperationException>(() =>
            {
                while (null != sut.GetNext())
                {
                }
            });
        }
    }
}
