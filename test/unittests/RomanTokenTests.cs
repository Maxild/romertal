﻿using System;
using System.Collections.Generic;
using System.Linq;
using Romertal;
using Xunit;

namespace UnitTests
{
    public class RomanTokenTests
    {
        [Fact]
        public void Eq()
        {
            Assert.False(RomanToken.I.Equals(RomanToken.C));
            Assert.False(RomanToken.I.EqualTo(RomanToken.C));
            Assert.True(RomanToken.I.Equals(RomanToken.I));
            Assert.True(RomanToken.I.EqualTo(RomanToken.I));
        }

        [Fact]
        public void Comparison()
        {
            Assert.True(RomanToken.I.LessThan(RomanToken.V));
            Assert.True(RomanToken.V.EqualTo(RomanToken.V));
            Assert.True(RomanToken.C.GreaterThan(RomanToken.X));
        }

        [Fact]
        public void GetAll()
        {
            Assert.True(RomanToken.GetAll().Select(x => x.Name).SequenceEqual(new [] {'I','V','X','L','C','D','M'}));
            Assert.True(RomanToken.GetAll().Select(x => x.Value).SequenceEqual(new [] {1,5,10,50,100,500,1000}));
        }
    }

    public static class BasicExtensions
    {
        public static bool LessThan<T>(this T x, T y) where T : IComparable<T>
        {
            return Comparer<T>.Default.Compare(x, y) < 0;
        }

        public static bool EqualTo<T>(this T x, T y) where T : IComparable<T>
        {
            return Comparer<T>.Default.Compare(x, y) == 0;
        }

        public static bool GreaterThan<T>(this T x, T y) where T : IComparable<T>
        {
            return Comparer<T>.Default.Compare(x, y) > 0;
        }
    }
}
