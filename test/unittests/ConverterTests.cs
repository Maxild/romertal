using System;
using Romertal;
using Xunit;

namespace UnitTests
{
    public class ConverterTests
    {
        [Fact]
        public void ContainsDecTokens()
        {
            Assert.True(Converter.ContainsDecTokens("123"));
            Assert.False(Converter.ContainsDecTokens("gh"));
            Assert.False(Converter.ContainsDecTokens("X"));
            Assert.False(Converter.ContainsDecTokens(string.Empty));
            Assert.False(Converter.ContainsDecTokens(null));
        }

        [Fact]
        public void ContainsRomanTokens()
        {
            Assert.True(Converter.ContainsRomanTokens("IVXLCDM"));
            Assert.False(Converter.ContainsRomanTokens("iVXLCDM")); // case sensitive
            Assert.False(Converter.ContainsRomanTokens("12"));
            Assert.False(Converter.ContainsRomanTokens(string.Empty));
            Assert.False(Converter.ContainsRomanTokens(null));
        }

        [Fact]
        public void DecToRoman_EmptyInput_ResultsInEmptyOutput()
        {
            Assert.Equal("", Converter.DecToRoman(""));
            Assert.Equal("", Converter.DecToRoman(null));
        }

        [Fact]
        public void DecToRoman()
        {
            Assert.Equal("MCMI", Converter.DecToRoman("1901"));
        }

        [Fact]
        public void RomanToDec_EmptyInput_ResultsInEmptyOutput()
        {
            Assert.Equal("", Converter.RomanToDec(""));
            Assert.Equal("", Converter.RomanToDec(null));
        }

        [Fact]
        public void RomanToDec()
        {
            // Valid values
            Assert.Equal("19", Converter.RomanToDec("XIX"));
            Assert.Equal("3999", Converter.RomanToDec("MMMCMXCIX"));
        }

        [Fact]
        public void Der_maa_kun_staa_eet_mindre_romertal_foran_et_stoerre()
        {
            Assert.Equal("Only one identical smaller digit must be found in front of a larger roman digit. The problem is at index 1.", Assert.Throws<InvalidOperationException>(() => Converter.RomanToDec("XIIX")).Message);
        }

        [Fact]
        public void flere_end_to_voksende_sekvenser_af_romertal_er_aldrig_ok()
        {
            Assert.Equal("An ascending sequence of roman digits 'IXC' of length greater than 2 have been found at index 2. Only (some) ascending pairs are allowed.", Assert.Throws<InvalidOperationException>(() => Converter.RomanToDec("MMIXC")).Message);
        }

        [Fact]
        public void alle_valide_mindre_foran_stoerre_virker()
        {
            Assert.Equal("4", Converter.RomanToDec("IV"));
            Assert.Equal("9", Converter.RomanToDec("IX"));
            Assert.Equal("40", Converter.RomanToDec("XL"));
            Assert.Equal("90", Converter.RomanToDec("XC"));
            Assert.Equal("400", Converter.RomanToDec("CD"));
            Assert.Equal("900", Converter.RomanToDec("CM"));
        }

        [Fact]
        public void invalide_mindre_foran_stoerre_throws()
        {
            Assert.Equal("The two ascending digits 'IL' at index 2 are not a valid pair.", Assert.Throws<InvalidOperationException>(() => Converter.RomanToDec("MDIL")).Message);
        }

        [Fact]
        public void nogen_tal_maa_gerne_repeteres_3gange()
        {
            Assert.Equal("3", Converter.RomanToDec("III"));
            Assert.Equal("30", Converter.RomanToDec("XXX"));
            Assert.Equal("300", Converter.RomanToDec("CCC"));
            Assert.Equal("3000", Converter.RomanToDec("MMM"));

            Assert.Equal("The roman digit 'I' cannot be repeated more then 3 times at index 0.", Assert.Throws<InvalidOperationException>(() => Converter.RomanToDec("IIII")).Message);
        }

        [Fact]
        public void andre_tal_maa_ikke_repeteres()
        {
            Assert.Equal("The roman digit 'D' cannot be repeated at index 0.", Assert.Throws<InvalidOperationException>(() => Converter.RomanToDec("DD")).Message);
        }
    }
}
